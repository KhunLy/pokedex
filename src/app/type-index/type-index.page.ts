import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActionSheetController, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-type-index',
  templateUrl: './type-index.page.html',
  styleUrls: ['./type-index.page.scss'],
})
export class TypeIndexPage implements OnInit {

  private List: Type[] = [];

  private NewName: string;

  constructor(
    private httpClient: HttpClient,
    private actionSheetCtrl: ActionSheetController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.httpClient.get<Type[]>('http://localhost:50596/api/Type')
      .subscribe(data => {
        this.List = data;
      }, error => {
        console.log(error);
      });
  }

  add() {
    this.httpClient.post<number>(
      'http://localhost:50596/api/Type', 
      <Type>{ Name: this.NewName })
      .subscribe(data => {
        this.List.push(<Type>{Id: data, Name: this.NewName});
        this.NewName = null;
      }, error => {
        console.log(error);
      });
  }
  async actions(item:Type){
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [{
        text: 'Modifier',
        icon: 'create',
        handler: async () => {
          let alert =await this.alertController.create({
            inputs: [{ name: 'Name', value: item.Name, label: 'Nom du type' }],
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'secondary'
            },{
              text: 'Confirmer',
              handler: (fields) => {
                // console.log(fields.Name);
                this.httpClient
                  .put<boolean>(
                    'http://localhost:50596/api/Type/' + item.Id, 
                    <Type>{Name: fields.Name}).subscribe(data => {
                      item.Name = fields.Name;
                    });
              }
            }]
          });
          alert.present();
        }
      },{
        text: 'Supprimer',
        icon: 'trash',
        handler: () => {
          // TODO
          // Contacter l'api et supprimer l'élément
          this.httpClient.delete('http://localhost:50596/api/Type/' + item.Id)
          .subscribe(data=>{
            let index = this.List.indexOf(item);
            this.List.splice(index, 1);
          }, error=>{console.log(error)});

        }
      }]
    });
    actionSheet.present();
  }

}
