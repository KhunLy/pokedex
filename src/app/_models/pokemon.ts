interface Pokemon{
    Id: number;
    Number: number;
    Name: string;
    Type1: string;
    Type2: string;
    Type1Id: number;
    Type2Id: number;
    Picture: string;
    PrevolutionId: number; 
}